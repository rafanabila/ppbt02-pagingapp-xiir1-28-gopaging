package com.example.rafanabila.gopaging.Database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.example.rafanabila.gopaging.Model.PemainModel

@Database(entities = [PemainModel::class], version = 1, exportSchema = false)
abstract class PemainDatabase : RoomDatabase() {
    abstract fun pemainDao(): PemainDao

    companion object {

        @Volatile
        private var INSTANCE: PemainDatabase? = null

        fun getInstance(context: Context): PemainDatabase =
                INSTANCE ?: synchronized(this) {
                    INSTANCE
                            ?: buildDatabase(context).also { INSTANCE = it }
                }

        private fun buildDatabase(context: Context) =
                Room.databaseBuilder(
                        context,
                        PemainDatabase::class.java, "badminton.db"
                ).allowMainThreadQueries().build()
    }
}