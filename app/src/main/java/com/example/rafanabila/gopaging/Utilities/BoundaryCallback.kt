package com.example.rafanabila.gopaging.Utilities

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.paging.PagedList
import android.util.Log
import com.example.rafanabila.gopaging.Model.PemainModel


class BoundaryCallback(val api: Api, val cache: LocalCache, val query: String) :
        PagedList.BoundaryCallback<PemainModel>() {
    private var lastRequestedPage = 1

    private val _networkErrors = MutableLiveData<String>()
    // LiveData of network errors.
    val networkErrors: LiveData<String>
        get() = _networkErrors

    // avoid triggering multiple requests in the same time
    private var isRequestInProgress = false

    override fun onZeroItemsLoaded() {
        requestAndSaveData(query)
    }

    override fun onItemAtEndLoaded(itemAtEnd: PemainModel) {
        requestAndSaveData(query)
    }

    private fun requestAndSaveData(query: String) {
        if (isRequestInProgress) return
        isRequestInProgress = true
        getPemain(api, lastRequestedPage, query, { pemain ->
            cache.insert(pemain)
            Log.d("Paging", "Load Page " + lastRequestedPage)
            isRequestInProgress = false
            lastRequestedPage = lastRequestedPage.inc()
        }, { error ->
            Log.d("Paging Error", error.toString())
            _networkErrors.postValue(error)
            isRequestInProgress = false
        })

    }
}