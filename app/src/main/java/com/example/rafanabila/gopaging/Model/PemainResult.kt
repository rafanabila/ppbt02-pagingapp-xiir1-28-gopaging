package com.example.rafanabila.gopaging.Model

import android.arch.lifecycle.LiveData
import android.arch.paging.PagedList

data class PemainResult(
        val data: LiveData<PagedList<PemainModel>>,
        val networkErrors: LiveData<String>
)