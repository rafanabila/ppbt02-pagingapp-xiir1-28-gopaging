package com.example.rafanabila.gopaging

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.arch.paging.PagedList
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.widget.SearchView
import android.widget.Toast
import com.example.rafanabila.gopaging.Model.PemainModel
import com.example.rafanabila.gopaging.Utilities.Injection
import com.example.rafanabila.gopaging.Utilities.PemainViewModel
import com.example.rafanabila.gopaging.Utilities.Preferences
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    lateinit var pemainViewModel: PemainViewModel
    lateinit var pref: SharedPreferences
    val adapter = Adapter()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        pref = getSharedPreferences(Preferences.PREFERENCES_NAME.value, Context.MODE_PRIVATE)

    }

    override fun onResume() {
        super.onResume()
        if (!pref.contains(Preferences.IP.value) || !pref.contains(Preferences.PORT.value)) {
            val i = Intent(this, SettingActivity::class.java)
            startActivity(i)
        } else {
            pemainViewModel = ViewModelProviders.of(
                    this,
                    Injection.provideViewModelFactory(this)
            )
                    .get(PemainViewModel::class.java)
            rv_pemain.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
            rv_pemain.layoutManager = LinearLayoutManager(this)

            rv_pemain.adapter = adapter
            pemainViewModel.searchRepo("")
            pemainViewModel.repos.observe(this, Observer<PagedList<PemainModel>> {
                adapter.submitList(it)
            })
            pemainViewModel.networkErrors.observe(this, Observer<String> {
                Toast.makeText(this, it!!, Toast.LENGTH_SHORT).show()
            })
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu!!)
        val searchItem = menu.findItem(R.id.search)
        val searchView = searchItem.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                rv_pemain.scrollToPosition(0)
                pemainViewModel.searchRepo(p0!!)
                adapter.submitList(null)
                return false
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                rv_pemain.scrollToPosition(0)
                pemainViewModel.searchRepo(p0!!)
                adapter.submitList(null)
                return false
            }
        })

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.setting -> {
                val i = Intent(this, SettingActivity::class.java)
                startActivity(i)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}

