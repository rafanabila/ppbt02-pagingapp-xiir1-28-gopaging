package com.example.rafanabila.gopaging.Model


import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "Pemain")
data class PemainModel(
        @PrimaryKey @field:SerializedName("Id") val Id: Int,
        @field:SerializedName("Nama") val Nama: String,
        @field:SerializedName("Negara") val Negara: String
)
