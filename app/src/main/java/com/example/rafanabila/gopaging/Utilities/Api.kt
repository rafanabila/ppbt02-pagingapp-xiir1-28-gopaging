package com.example.rafanabila.gopaging.Utilities

import android.util.Log
import com.example.rafanabila.gopaging.Model.PemainModel
import com.example.rafanabila.gopaging.Model.PemainResponse
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

fun getPemain(
        service: Api,
        page: Int,
        query: String,
        onSuccess: (repos: List<PemainModel>) -> Unit,
        onError: (error: String) -> Unit
) {

    Log.d("API", service.getPemain(page, query).toString())
    service.getPemain(page, query).enqueue(

            object : Callback<PemainResponse> {
                override fun onFailure(call: Call<PemainResponse>?, t: Throwable) {
                    onError(t.message ?: "unknown error")
                }

                override fun onResponse(
                        call: Call<PemainResponse>?,
                        response: Response<PemainResponse>
                ) {
                    if (response.isSuccessful) {
                        val repos = response.body()?.Data ?: emptyList()
                        onSuccess(repos)
                    } else {
                        onError(response.errorBody()?.string() ?: "Unknown error")
                    }
                }
            }
    )
}

interface Api {

    @GET("/pemains")
    fun getPemain(@Query("page") page: Int, @Query("search") search: String): Call<PemainResponse>
    companion object {
        fun getService(ip: String): Api {
            val logger = HttpLoggingInterceptor()
            logger.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient.Builder()
                    .addInterceptor(logger)
                    .build()
            val retrofit = Retrofit.Builder().baseUrl("http://" + ip)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            return retrofit.create(Api::class.java)
        }
    }

}
