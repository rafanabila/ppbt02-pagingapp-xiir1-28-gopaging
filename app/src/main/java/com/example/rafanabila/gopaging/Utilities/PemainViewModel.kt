package com.example.rafanabila.gopaging.Utilities


import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import android.arch.paging.PagedList
import com.example.rafanabila.gopaging.Model.PemainModel
import com.example.rafanabila.gopaging.Model.PemainResult

class PemainViewModel(val repository: PemainRepository) : ViewModel() {
    private val queryLiveData = MutableLiveData<String>()
    private val repoResult: LiveData<PemainResult> = Transformations.map(queryLiveData, {
        repository.search(it)
    })

    val repos: LiveData<PagedList<PemainModel>> = Transformations.switchMap(repoResult,
            { it -> it.data })
    val networkErrors: LiveData<String> = Transformations.switchMap(repoResult,
            { it -> it.networkErrors })

    fun searchRepo(queryString: String) {
        queryLiveData.postValue(queryString)
    }
}