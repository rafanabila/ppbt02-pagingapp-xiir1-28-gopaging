package com.example.rafanabila.gopaging

import android.arch.paging.PagedListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.rafanabila.gopaging.Model.PemainModel
import kotlinx.android.synthetic.main.rv_layout.view.*

class Adapter : PagedListAdapter<PemainModel, Adapter.PemainViewHolder>(
        DiffCallback
) {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): PemainViewHolder {
        return PemainViewHolder(
                LayoutInflater.from(p0.context).inflate(
                        R.layout.rv_layout,
                        p0,
                        false
                )
        )
    }

    override fun onBindViewHolder(p0: PemainViewHolder, p1: Int) {
        val pemain = getItem(p1)
        if (pemain != null) {
            p0.bind(pemain)
        }
    }

    companion object {
        val DiffCallback = object : DiffUtil.ItemCallback<PemainModel>() {
            override fun areItemsTheSame(p0: PemainModel, p1: PemainModel): Boolean {
                return p0.Id == p1.Id
            }

            override fun areContentsTheSame(p0: PemainModel, p1: PemainModel): Boolean {
                return p0 == p1
            }

        }
    }

    class PemainViewHolder(val v: View) : RecyclerView.ViewHolder(v) {
        fun bind(m: PemainModel) {
            v.tv_name.text = m.Nama
            v.tv_negara.text = m.Negara
        }
    }
}