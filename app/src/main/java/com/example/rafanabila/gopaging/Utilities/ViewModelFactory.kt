package com.example.rafanabila.gopaging.Utilities

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider

class ViewModelFactory(val repo: PemainRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PemainViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return PemainViewModel(repo) as T
        }
        throw IllegalArgumentException("Unknown PemainViewModel class")
    }
}