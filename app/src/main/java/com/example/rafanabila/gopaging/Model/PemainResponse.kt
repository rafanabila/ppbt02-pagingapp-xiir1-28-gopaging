package com.example.rafanabila.gopaging.Model

import com.google.gson.annotations.SerializedName

data class PemainResponse(
        @SerializedName("NextPage") val NextPage: Int = 0,
        @SerializedName("Data") val Data: List<PemainModel> = emptyList()
)