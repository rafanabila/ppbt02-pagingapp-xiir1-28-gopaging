package com.example.rafanabila.gopaging.Utilities

enum class Preferences(val value: String) {
    PREFERENCES_NAME("mhw_preferences"),
    IP("IP_address"),
    PORT("Port")
}