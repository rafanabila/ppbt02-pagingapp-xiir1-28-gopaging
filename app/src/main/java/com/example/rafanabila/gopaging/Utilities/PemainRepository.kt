package com.example.rafanabila.gopaging.Utilities

import android.arch.paging.LivePagedListBuilder
import com.example.rafanabila.gopaging.Model.PemainResult

class PemainRepository(val api: Api, val cache: LocalCache) {
    fun search(query: String): PemainResult {
        val dataSourceFactory = cache.getData(query)
        val boundaryCallback = BoundaryCallback(api, cache, query)
        val data = LivePagedListBuilder(dataSourceFactory, 10)
                .setBoundaryCallback(boundaryCallback)
                .build()
        return PemainResult(data, boundaryCallback.networkErrors)
    }
}