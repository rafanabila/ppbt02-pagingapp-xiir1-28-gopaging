package com.example.rafanabila.gopaging.Utilities

import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import com.example.rafanabila.gopaging.Database.PemainDatabase
import java.util.concurrent.Executors

object Injection {
    private fun provideCache(context: Context): LocalCache {
        val database = PemainDatabase.getInstance(context)
        return LocalCache(
                database.pemainDao(),
                Executors.newSingleThreadExecutor()
        )
    }


    private fun providePemainRepository(context: Context): PemainRepository {
        val pref = context.getSharedPreferences(Preferences.PREFERENCES_NAME.value, Context.MODE_PRIVATE)
        val ip = pref.getString(Preferences.IP.value, "")
        val port = pref.getString(Preferences.PORT.value, "")
        return PemainRepository(
                Api.getService(
                        ip + ":" + port
                ), provideCache(context)
        )
    }


    fun provideViewModelFactory(context: Context): ViewModelProvider.Factory {
        return ViewModelFactory(
                providePemainRepository(
                        context
                )
        )
    }
}