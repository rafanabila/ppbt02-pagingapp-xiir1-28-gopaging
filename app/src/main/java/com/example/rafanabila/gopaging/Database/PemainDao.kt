package com.example.rafanabila.gopaging.Database

import android.arch.paging.DataSource
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.example.rafanabila.gopaging.Model.PemainModel


@Dao
interface PemainDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(posts: List<PemainModel>)

    @Query("SELECT * FROM pemain where (Nama LIKE :queryString) ORDER BY Nama ASC")
    fun getPemain(queryString: String): DataSource.Factory<Int, PemainModel>

}