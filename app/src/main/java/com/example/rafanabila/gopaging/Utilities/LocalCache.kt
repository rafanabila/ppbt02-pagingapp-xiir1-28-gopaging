package com.example.rafanabila.gopaging.Utilities

import android.arch.paging.DataSource
import com.example.rafanabila.gopaging.Database.PemainDao
import com.example.rafanabila.gopaging.Model.PemainModel
import java.util.concurrent.Executor

class LocalCache(val pemainDao: PemainDao, val executor: Executor) {
    fun insert(repos: List<PemainModel>) {
        executor.execute {
            pemainDao.insert(repos)
        }
    }

    fun getData(nama: String): DataSource.Factory<Int, PemainModel> {
        return pemainDao.getPemain("%${nama.replace(' ', '%')}%")
    }
}