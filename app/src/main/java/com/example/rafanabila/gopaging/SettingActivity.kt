package com.example.rafanabila.gopaging

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent
import android.view.View
import android.widget.Toast
import com.example.rafanabila.gopaging.Utilities.Preferences
import kotlinx.android.synthetic.main.activity_setting.*

class SettingActivity : AppCompatActivity() {
    lateinit var pref: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        pref = getSharedPreferences(Preferences.PREFERENCES_NAME.value, Context.MODE_PRIVATE)
        var first = true
        if (pref.contains(Preferences.IP.value)) {
            first = false
            val ip = pref.getString(Preferences.IP.value, "")!!.split(".")
            et_seg1.setText(ip.get(0))
            et_seg2.setText(ip.get(1))
            et_seg3.setText(ip.get(2))
            et_seg4.setText(ip.get(3))
        }
        et_seg1.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(p0: View?, p1: Int, p2: KeyEvent?): Boolean {
                if (et_seg1.text.toString().length >= 3) {
                    et_seg2.requestFocus()
                }
                return false
            }
        })
        et_seg2.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(p0: View?, p1: Int, p2: KeyEvent?): Boolean {
                if (et_seg2.text.toString().length >= 3) {
                    et_seg3.requestFocus()
                }
                return false
            }
        })
        et_seg3.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(p0: View?, p1: Int, p2: KeyEvent?): Boolean {
                if (et_seg3.text.toString().length >= 3) {
                    et_seg4.requestFocus()
                }
                return false
            }
        })
        et_seg4.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(p0: View?, p1: Int, p2: KeyEvent?): Boolean {
                if (et_seg4.text.toString().length >= 3) {
                    et_port.requestFocus()
                }
                return false
            }
        })
        et_port.setText(pref.getString(Preferences.PORT.value, ""))
        btn_save.setOnClickListener {
            val editor = pref.edit()
            val ipAddress =
                    et_seg1.text.toString() + "." + et_seg2.text.toString() + "." + et_seg3.text.toString() + "." + et_seg4.text.toString()
            val port = et_port.text.toString()
            editor.putString(Preferences.IP.value, ipAddress)
            editor.putString(Preferences.PORT.value, port)
            editor.apply()
            Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show()
            if (first) {
                val i = Intent(this, MainActivity::class.java)
                startActivity(i)
                finish()
            }
        }
    }
}
